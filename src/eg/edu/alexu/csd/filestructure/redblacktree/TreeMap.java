package eg.edu.alexu.csd.filestructure.redblacktree;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.management.RuntimeErrorException;

public class TreeMap <T extends Comparable<T>, V>implements ITreeMap<T,V>{
    RedBlackTree<T, V> tree=new RedBlackTree<>();
    int size=0;
	@Override
	public Entry<T, V> ceilingEntry(T key) {
		if(key==null) {throw new RuntimeErrorException(null);}
		if(tree.getRoot().isNull()) {return null;}
		INode<T, V> temp=tree.getRoot();
		while(!temp.isNull()) {
			if(temp.getKey().equals(key)) {break;}
			else if(temp.getKey().compareTo(key)<0) {temp=temp.getRightChild();}
			else {INode tINode=temp;
				 temp=temp.getLeftChild();
			      if(temp==null) {
			    	  temp=tINode;
			    	  break;}}
			}
			Entry<T,V> entry=new MyEntry(temp.getKey(), temp.getValue());
			return entry;
	}

	@Override
	public T ceilingKey(T key) {
		Entry<T,V> e=ceilingEntry(key);
		if(e==null) {return null;}
		return e.getKey();
	}

	@Override
	public void clear() {
		tree.clear();
		size=0;
	}

	@Override
	public boolean containsKey(T key) {
		return tree.contains(key);
	}

	@Override
	public boolean containsValue(V value) {
		if(value==null) { throw new RuntimeErrorException(null); }
		if(tree.isEmpty()) { return false; }
		for(Entry<T,V> entry : entrySet()) {
			if(entry.getValue().equals(value)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public Set<Entry<T, V>> entrySet() {
		Set <Entry<T, V>> arr= new LinkedHashSet<Entry<T,V>>();
		InorderTraverse(arr,tree.getRoot());
		return arr;
	}
    public void InorderTraverse(Set <Entry<T, V>> arr,INode<T, V> n) {
    	if(n.isNull()) {return;}
    	InorderTraverse(arr, n.getLeftChild());
    	arr.add(new AbstractMap.SimpleEntry<T,V>(n.getKey(), n.getValue()));
    	InorderTraverse(arr, n.getRightChild());
    }
    @Override
	public Entry<T, V> firstEntry() {
		if(tree.isEmpty()) { return null; }
		T min = tree.getRoot().getKey();
		for(Entry<T,V> temp : entrySet()) {
			if(temp.getKey().compareTo(min)<0) {
				min = temp.getKey();
			}
		}
		Entry<T,V> entry=new MyEntry(min, get(min));
		return entry;
	}

	@Override
	public T firstKey() {
		if(tree.isEmpty()) { return null; }
		T min = tree.getRoot().getKey();
		for(Entry<T,V> temp : entrySet()) {
			if(temp.getKey().compareTo(min)<0) {
				min = temp.getKey();
			}
		}
		return min;
	}

	@Override
	public Entry<T, V> floorEntry(T key) {
		if(key==null) {throw new RuntimeErrorException(null);}
		if(tree.getRoot().isNull()) {return null;}
		INode<T, V> temp=tree.getRoot();
		while(!temp.isNull()) {
			if(temp.getKey().equals(key)) {break;}
			else if(temp.getKey().compareTo(key)>0) {temp=temp.getLeftChild();}
			else {INode tINode=temp;
				 temp=temp.getRightChild();
			      if(temp==null) {
			    	  temp=tINode;
			    	  break;}}
			}
			Entry<T,V> entry=new MyEntry(temp.getKey(), temp.getValue());
			return entry;
	}

	@Override
	public T floorKey(T key) {
		Entry<T,V> e=floorEntry(key);
		if(e==null) {return null;}
		return e.getKey();
	}

	@Override
	public V get(T key) {
		return tree.search(key);
	}

	@Override
	public ArrayList<Entry<T, V>> headMap(T toKey) {
		return headMap(toKey,false);
	}

	@Override
	public Set<T> keySet() {
		Set <Entry<T, V>> arr= entrySet();
		if(arr==null) {return null;}
		Set <T> keys= new LinkedHashSet<T>();
		for(Entry<T, V> entry:arr) {
			keys.add(entry.getKey());
		}
		return keys;
	}

	@Override
	public Entry<T, V> lastEntry() {
		if(tree.isEmpty()) { return null; }
		T max = tree.getRoot().getKey();
		for(Entry<T,V> temp : entrySet()) {
			if(temp.getKey().compareTo(max)>0) {
				max = temp.getKey();
			}
		}
		Entry<T,V> entry=new MyEntry(max, get(max));
		return entry;
	}

	@Override
	public T lastKey() {
		if(tree.isEmpty()) { return null; }
		T last = tree.getRoot().getKey();
		for(Entry<T,V> entry : entrySet()) {
			last = entry.getKey();
		}
		return last;
	}

	@Override
	public Entry<T, V> pollFirstEntry() {
		if(tree.isEmpty()) { return null; }
		Entry<T,V> entry=new MyEntry(firstEntry().getKey(), firstEntry().getValue());
		remove(firstEntry().getKey());
		size--;
		return entry;
	}

	@Override
	public Entry<T, V> pollLastEntry() {
		if(tree.isEmpty()) { return null; }
		Entry<T,V> entry=new MyEntry(lastEntry().getKey(), lastEntry().getValue());
		remove(lastEntry().getKey());
		size--;
		return entry;
	}

	@Override
	public void put(T key, V value) {
		boolean f=false;
		if(tree.contains(key)) {f=true;}
		tree.insert(key, value);
		if(!f) {size++;}
	}

	@Override
	public void putAll(Map<T, V> map) {
		if(map==null) {throw new RuntimeErrorException(null);}
		for(Entry<T, V> entry:map.entrySet()) {
			tree.insert(entry.getKey(), entry.getValue());
			size++;
		}
	}

	@Override
	public boolean remove(T key) {
		return tree.delete(key);
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public Collection<V> values() {
		if(tree.isEmpty()) {throw new RuntimeErrorException(null);}
		Collection<V> coll = new ArrayList<V>();
		for(Entry<T,V> entry : entrySet()) {
			coll.add(entry.getValue());
		}
		return coll;
	}
private class MyEntry implements Entry<T, V>{
T key=null;
V value=null;
public MyEntry(T k,V v) {
	key=k;
	value=v;
}
	@Override
	public T getKey() {
		return key;
	}

	@Override
	public V getValue() {
		return value;
	}

	@Override
	public V setValue(V arg0) {
		return null;
	}
	
}
@Override
public ArrayList<Entry<T, V>> headMap(T toKey, boolean inclusive) {
	if(toKey==null) {throw new RuntimeErrorException(null);}
	if(tree.getRoot().isNull()) {return null;}
	ArrayList<Entry<T, V>> ans=new ArrayList<Entry<T, V>> ();
	Set <Entry<T, V>> arr= entrySet();
	for(Entry<T, V> entry:arr) {
		if(entry.getKey().compareTo(toKey)<0) {ans.add(entry);}
		else if(entry.getKey().equals(toKey)&&inclusive) {ans.add(entry);}
		else {break;}
	}
	return ans;
}
}