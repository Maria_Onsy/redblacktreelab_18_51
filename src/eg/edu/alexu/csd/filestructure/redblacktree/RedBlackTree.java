package eg.edu.alexu.csd.filestructure.redblacktree;

import javax.management.RuntimeErrorException;

public class RedBlackTree <T extends Comparable<T>, V> implements IRedBlackTree<T, V>{
	INode<T, V> root=new Node<T,V>();
	@Override
	public INode<T, V> getRoot() {
		if(root.isNull()){return root;}
		return root;
	}

	@Override
	public boolean isEmpty() {
		return root.isNull();
	}

	@Override
	public void clear() {        
		if(isEmpty()) {return;}
		root.setValue(null);
		root.setKey(null);
		root.setLeftChild(null);
		root.setRightChild(null);
	}

	@Override
	public V search(T key) {
		if(key==null) {throw new RuntimeErrorException(null);}
		if(root.isNull()) {return null;}
		INode<T, V> temp=root;
		while(!temp.isNull()) {
			if(temp.getKey().equals(key)) {return temp.getValue();}
			else if(temp.getKey().compareTo(key)<0) {temp=temp.getRightChild();}
			else if(temp.getKey().compareTo(key)>0) {temp=temp.getLeftChild();}
		}
		return null;
	}

	@Override
	public boolean contains(T key) {
		V temp=search(key);
		if(temp==null) {return false;}
		return true;
	}

	@Override
	public void insert(T key, V value) {
		if(key==null||value==null) {throw new RuntimeErrorException(null);}
		INode<T,V> x=new Node<T,V>();
		x.setKey(key);
		x.setValue(value);
		x.setLeftChild(new Node<T,V>());
		x.setRightChild(new Node<T,V>());
		if(root.isNull()) {
			root=x;
			root.setColor(false);
			return;
		}
		INode<T, V> temp=root;
		INode<T, V> next=root;
		while(!next.isNull()) {
			temp=next;
			if(temp.getKey().equals(key)) {temp.setValue(value); return;}
			else if(temp.getKey().compareTo(key)<0) {next=temp.getRightChild();}
			else {next=temp.getLeftChild();}
		}
		if(temp.getKey().compareTo(key)<0) {temp.setRightChild(x);}
		else {temp.setLeftChild(x);}
		x.setParent(temp);
		x.setColor(true);
		insertFixup(x);
	}

	@Override
	public boolean delete(T key) {
		if(key==null) {throw new RuntimeErrorException(null);}
		V temp = search(key);
		if (temp==null) { return false; }
		INode<T,V> node = Node(key);
		BSTD(node);
		return true;
	}
	
	private INode<T,V> Node(T key){
		if(key==null) {throw new RuntimeErrorException(null);}
		if(root.isNull()) {return null;}
		INode<T, V> temp=root;
		while(!temp.isNull()) {
			if(temp.getKey().equals(key)) {return temp;}
			else if(temp.getKey().compareTo(key)<0) {temp=temp.getRightChild();}
			else if(temp.getKey().compareTo(key)>0) {temp=temp.getLeftChild();}
		}
		return null;
	}
	
	private void BSTD (INode<T,V> node) {
		if(node.getLeftChild().isNull() && node.getRightChild().isNull()) { DeleteLeafNode(node); return;}
		if(!node.getLeftChild().isNull() || !node.getRightChild().isNull()) { DeleteOneChildNode(node); }
		if(!node.getLeftChild().isNull() && !node.getRightChild().isNull()) { DeleteInnerNode(node); return;}
	}

	private void DeleteLeafNode(INode<T,V> node) {
		if(node.equals(getRoot())) {node.setValue(null); node.setKey(null); return;}
		else if(!node.getParent().getLeftChild().isNull() && node.getParent().getLeftChild().equals(node)) { 
			if(node.getColor()) { 
				node.getParent().setLeftChild(node.getLeftChild());
			}	
			else { fixDelete(node); 
				node.getParent().setLeftChild(node.getLeftChild());
			}		
		}
		else if(!node.getParent().getRightChild().isNull() && node.getParent().getRightChild().equals(node)){
			if(node.getColor()) { 
				node.getParent().setRightChild(node.getRightChild());
			}	
			else { fixDelete(node); 
				node.getParent().setRightChild(node.getRightChild());
			}
		}
	}
	
	private void DeleteOneChildNode(INode<T,V> node) {
		if(node.equals(getRoot()) && (node.getLeftChild().isNull()||node.getRightChild().isNull())) {
			Node<T,V> n;
			if (!node.getRightChild().isNull()) { 
				n=(Node<T, V>) FindInorderSuccessor(node); 
				node.setValue(n.getValue()); node.setKey(n.getKey());
				if(!n.getRightChild().isNull()) { DeleteOneChildNode(n); }
				else { DeleteLeafNode(n); }
			}else if(!node.getLeftChild().isNull()) {
				n=(Node<T, V>) FindInorderPredecessor(node);
				node.setValue(n.getValue()); node.setKey(n.getKey());
				if(!n.getLeftChild().isNull()) { DeleteOneChildNode(n); }
				else { DeleteLeafNode(n); }
			}else { node.setKey(null); node.setValue(null); }
			return;
		}
		if(!node.getLeftChild().isNull() && node.getRightChild().isNull()) {
			if( !node.getParent().getLeftChild().isNull() && node.getParent().getLeftChild().equals(node)) {
				if(node.getColor()) { node.getParent().setLeftChild(node.getLeftChild()); node.getLeftChild().setParent(node.getParent());}
				else {
					if(node.getLeftChild().getColor()) { node.getLeftChild().setColor(false); node.getParent().setLeftChild(node.getLeftChild()); node.getLeftChild().setParent(node.getParent());}
					else {  node.getParent().setLeftChild(node.getLeftChild()); fixDelete(node.getLeftChild());node.getLeftChild().setParent(node.getParent());}
				}
			}else {
				if(node.getColor()) { node.getParent().setRightChild(node.getLeftChild()); node.getLeftChild().setParent(node.getParent());}
				else {
					if(node.getLeftChild().getColor()) { node.getLeftChild().setColor(false); node.getParent().setRightChild(node.getLeftChild()); node.getLeftChild().setParent(node.getParent());}
					else {  node.getParent().setRightChild(node.getLeftChild()); fixDelete(node.getLeftChild());node.getLeftChild().setParent(node.getParent());}
				}
			}
			
		}else if(node.getLeftChild().isNull() && !node.getRightChild().isNull()) {
			if(!node.getParent().getLeftChild().isNull() && node.getParent().getLeftChild().equals(node)) {
				if(node.getColor()) { node.getParent().setLeftChild(node.getRightChild()); node.getRightChild().setParent(node.getParent());}
				else {
					if(node.getRightChild().getColor()) { node.getRightChild().setColor(false); node.getParent().setLeftChild(node.getRightChild()); node.getRightChild().setParent(node.getParent());}
					else {  node.getParent().setLeftChild(node.getRightChild()); fixDelete(node.getRightChild());node.getRightChild().setParent(node.getParent());}
				}
			}else {
				if(node.getColor()) { node.getParent().setRightChild(node.getRightChild()); node.getRightChild().setParent(node.getParent());}
				else {
					if(node.getRightChild().getColor()) { node.getRightChild().setColor(false); node.getParent().setRightChild(node.getRightChild()); node.getRightChild().setParent(node.getParent());}
					else {  node.getParent().setRightChild(node.getRightChild()); fixDelete(node.getRightChild());node.getRightChild().setParent(node.getParent());}
				}
			}
		}
	}
	
	private void DeleteInnerNode(INode<T,V> node) {
		INode<T,V> n;
		if (!node.getRightChild().isNull()) { 
			n=FindInorderSuccessor(node); 
			node.setValue(n.getValue()); node.setKey(n.getKey());
			if(!n.getRightChild().isNull()) { DeleteOneChildNode(n); }
			else { DeleteLeafNode(n); }
		}else {
			n=FindInorderPredecessor(node);
			node.setValue(n.getValue()); node.setKey(n.getKey());
			if(!n.getLeftChild().isNull()) { DeleteOneChildNode(n); }
			else { DeleteLeafNode(n); }
		}
	}
	
	private INode<T,V> FindInorderSuccessor(INode<T,V> node) {
		INode<T,V> min = node.getRightChild();
		while(!min.getLeftChild().isNull()) {
			min=min.getLeftChild();
		}return min;
	}
	
	private INode<T,V> FindInorderPredecessor(INode<T,V> node) {
		INode<T,V> max = node.getLeftChild();
		while(!max.getRightChild().isNull()) {
			max=max.getRightChild();
		}return max;
	}
	
	private void fixDelete(INode<T,V> node) {
		if(node.equals(getRoot())) { node.setColor(false); return; }
		INode<T,V> parent = node.getParent();
		if(!node.getParent().getRightChild().isNull() && parent.getLeftChild().equals(node) ) {
			INode<T,V> sibling = node.getParent().getRightChild();
			if(!sibling.getColor() && parent.getColor()) {
				if((sibling.getLeftChild().isNull() || !sibling.getLeftChild().getColor())&&(sibling.getRightChild().isNull() || !sibling.getRightChild().getColor())) {
					 parent.setColor(false); sibling.setColor(true); return;
				}
			}
			if(!sibling.getColor() && !parent.getColor()) {
				if((sibling.getLeftChild().isNull() || !sibling.getLeftChild().getColor())&&(sibling.getRightChild().isNull() || !sibling.getRightChild().getColor())) {
					 sibling.setColor(true); fixDelete(parent);  return;
				}
			}
			if(sibling.getColor() ){
				boolean temp = parent.getColor();
				parent.setColor(sibling.getColor()); sibling.setColor(temp);
				 rotateLeft(parent); fixDelete(node); return;
			}
			if(!sibling.getColor() && sibling.getLeftChild().getColor() && (!sibling.getRightChild().getColor() || sibling.getRightChild().isNull())) {
				boolean temp = sibling.getColor();
				sibling.setColor(sibling.getLeftChild().getColor()); sibling.getLeftChild().setColor(temp);
				 rotateRight(sibling); fixDelete(node); return;
			}
			if(!sibling.getColor() && sibling.getRightChild().getColor()) {
				boolean temp = parent.getColor();
				parent.setColor(sibling.getColor()); sibling.setColor(temp);
				sibling.getRightChild().setColor(false); 
				rotateLeft(parent); return;
			}
			
		}
		if(parent.getRightChild().equals(node) && !node.getParent().getLeftChild().isNull()) {
			INode<T,V> sibling = node.getParent().getLeftChild();
			if(!sibling.getColor() && parent.getColor()) {
				if((sibling.getLeftChild().isNull() || !sibling.getLeftChild().getColor())&&(sibling.getRightChild().isNull() || !sibling.getRightChild().getColor())) {
					/*node.setDB(0);*/ parent.setColor(false); sibling.setColor(true); return;
				}
			}
			if(!sibling.getColor() && !parent.getColor()) {
				if((sibling.getLeftChild().isNull() || !sibling.getLeftChild().getColor())&&(sibling.getRightChild().isNull() || !sibling.getRightChild().getColor())) {
					 sibling.setColor(true); fixDelete(parent);  return;
				}
			}
			if(sibling.getColor() ){
				boolean temp = parent.getColor();
				parent.setColor(sibling.getColor()); sibling.setColor(temp);
				 rotateRight(parent); fixDelete(node); return;
			}
			if(!sibling.getColor() && sibling.getRightChild().getColor() && (!sibling.getLeftChild().getColor() || sibling.getLeftChild().isNull())) {
				boolean temp = sibling.getColor();
				sibling.setColor(sibling.getRightChild().getColor()); sibling.getRightChild().setColor(temp);
				 rotateLeft(sibling); fixDelete(node); return;
			}
			if(!sibling.getColor() && sibling.getLeftChild().getColor()) {
				boolean temp = parent.getColor();
				parent.setColor(sibling.getColor()); sibling.setColor(temp);
				sibling.getLeftChild().setColor(false); 
				rotateRight(parent); return;
			}
			
		}
		
	}
	
	public void rotateLeft(INode<T, V> x) {
				INode<T, V> y=x.getRightChild();
				INode<T, V> p=x.getParent();
				if(!y.getLeftChild().isNull()) {
					x.setRightChild(y.getLeftChild());
					y.getLeftChild().setParent(x);}
				else {x.setRightChild(new Node<T,V>());}
				if(p==null) {
					root=y;
					root.setParent(null);
					root.setLeftChild(x);
					x.setParent(root);
					}
				else if(!p.getLeftChild().isNull()&&p.getLeftChild().getKey().equals(x.getKey())) {
					p.setLeftChild(y);
					y.setParent(p);}
				else {
					p.setRightChild(y);
					y.setParent(p);
					}
				y.setLeftChild(x);
				x.setParent(y);
			}
	public void rotateRight(INode<T, V> x) {
		   	INode<T, V> y=x.getLeftChild();
			INode<T, V> p=x.getParent();
			if(!y.getRightChild().isNull()) {
				x.setLeftChild(y.getRightChild());
				y.getRightChild().setParent(x);}
			else {x.setLeftChild(new Node<T,V>());}
				if(p==null) {
					root=y;
					root.setParent(null);
				    root.setRightChild(x);
					x.setParent(root);
			    }
				else if(!p.getLeftChild().isNull()&&p.getLeftChild().getKey().equals(x.getKey())) {
					p.setLeftChild(y);
					y.setParent(p);}
				else {
					p.setRightChild(y);
					y.setParent(p);}
				y.setRightChild(x);
				x.setParent(y);
			}
	public void insertFixup(INode<T, V> x) {
		if(x.getKey()==root.getKey()) {
	   		root.setColor(false);
	   		return;}
		   	INode<T, V> p=x.getParent();
	 	if(!p.getColor()) {return;}
	    	INode<T, V> gp=x.getParent().getParent();
	    	if(gp==null) {
		   		x.setColor(false);
		   		return;}
		   	INode<T, V> unc=new Node<T,V>();
			boolean f=false;
	    	if(!gp.getLeftChild().isNull()&&gp.getLeftChild().getKey().equals(p.getKey())) {
		    	unc=gp.getRightChild();
		   		f=true;
		   	}
		   	else {unc=gp.getLeftChild();}
		   	if(unc.getColor()) {
	    		unc.setColor(false);
	    		p.setColor(false);
	    		gp.setColor(true);
		    	insertFixup(gp);
		   	}
		    	//left-right
		   	else if(!p.getRightChild().isNull()&&p.getRightChild().getKey().equals(x.getKey())&&f) {
		   		rotateLeft(p);
		   		insertFixup(p);}
		   		//right-left
		    else if(!p.getLeftChild().isNull()&&p.getLeftChild().getKey().equals(x.getKey())&&!f){
		   		rotateRight(p);
		   		insertFixup(p);
		   	}
		   	//left-left
	    	else if(!p.getLeftChild().isNull()&&p.getLeftChild().getKey().equals(x.getKey())&&f){
	    		p.setColor(false);
	    		gp.setColor(true);
		    	rotateRight(gp);
		   	}
		   	//right-right
		   	else {
		   		p.setColor(false);
	    		gp.setColor(true);
	    		rotateLeft(gp);
	    	}
	}
}
